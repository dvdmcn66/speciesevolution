from random import randint, uniform

import numpy as np

from conf import COORD_MAX_X, COORD_MAX_Y, HEIGHT_MAX_VALUE, VALUE_MAX_HIDE, INIT_FOOD, MAX_FOOD


def init_animals():
    # [x, y, height, food] ; food==0 => die
    return [randint(0, COORD_MAX_X), randint(0, COORD_MAX_Y), uniform(0, HEIGHT_MAX_VALUE), INIT_FOOD,
            uniform(0, MAX_FOOD), uniform(0, VALUE_MAX_HIDE), uniform(0, 1)]


def init_population(nb_animals):
    animals = []
    for i in range(nb_animals):
        animals.append(init_animals())
    return np.array(animals)
