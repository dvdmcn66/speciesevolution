import matplotlib.pyplot as plt
import time

from action import aleatory_move, predators_find_preys, make_child, make_predator_hungry
from conf import ANIMALS_POP, COORD_MAX_X, COORD_MAX_Y, EPOCH, X_INDICE, Y_INDICE, HEIGHT_INDICE, HIDE_INDICE, \
    THRESHOLD_HUNGRY_TO_HUNT,EAT_ANIMAL, MIN_ALEATORY_COLD_FACTOR, MAX_ALEATORY_COLD_FACTOR
from init_function import init_population
from stats_and_graph import compute_stats, color_animal
from random import uniform

if __name__ == '__main__':
    height_mean_preys = []
    height_mean_predators = []
    height_mean_omnnis = []
    hide_mean = []
    predators_pop = []
    preys_pop = []
    omnis_pop = []
    threshold_to_hunt = []
    ecarttype_eat_animal=[]
    animals = init_population(ANIMALS_POP)
    aleatory_factor = 1
    for i in range(EPOCH):
        if i % 100 == 1:
            print(i, "epochs -- ", predators_pop[-1], " predators -- ", omnis_pop[-1], " omnis - ", preys_pop[-1], " preys === aleatory factor hungry = ", aleatory_factor)
        if i % 2000 == 0 and i > 1000:
            aleatory_factor = uniform(MIN_ALEATORY_COLD_FACTOR, MAX_ALEATORY_COLD_FACTOR)
        if animals.shape[0] < 2:
            print("STOOOP", i)
            break
        plt.cla()
        threshold_to_hunt, height_mean_preys, height_mean_predators, hide_mean, predators_pop, preys_pop, ecarttype_eat_animal, omnis_pop, height_mean_omnnis\
            = compute_stats(
            threshold_to_hunt, animals, height_mean_preys, height_mean_predators, hide_mean, predators_pop,
            preys_pop, ecarttype_eat_animal, omnis_pop, height_mean_omnnis)

        color_animals, size_animals = color_animal(animals)

        plt.scatter(x=animals[:, X_INDICE], y=animals[:, Y_INDICE], c=color_animals, s=size_animals)
        animals = aleatory_move(animals)

        plt.gca().set_facecolor((0, 0.5, 0))
        plt.xlim(-10, COORD_MAX_X + 10)
        plt.ylim(-10, COORD_MAX_Y + 10)
        plt.pause(0.001)
        plt.draw()
        animals = predators_find_preys(animals)
        animals = make_child(animals)
        animals = make_predator_hungry(animals, animals.shape[0], aleatory_factor)
    time.sleep(5)

    plt.figure(2)
    plt.plot(height_mean_preys, label="height_mean_preys")
    plt.plot(height_mean_predators, label="height_mean_predators")
    plt.plot(height_mean_omnnis, label="height_mean_omnnis")
    plt.plot(hide_mean, label="hide_mean")
    plt.legend()
    plt.figure(3)
    plt.plot(predators_pop, label="predators_pop")
    plt.plot(preys_pop, label="preys_pop")
    plt.plot(omnis_pop, label="omnis_pop")
    plt.legend()
    plt.figure(4)
    plt.scatter(animals[0:100, EAT_ANIMAL], animals[0:100, THRESHOLD_HUNGRY_TO_HUNT], label="abscisses = carnivorisme ; ordonnées = threshold hungry to hunt")
    plt.legend()
    plt.figure(5)
    plt.show()
    plt.figure(6)
    plt.plot(threshold_to_hunt, label="threshold_to_hunt")
    plt.legend()
    plt.figure(7)
    #plt.plot(ecarttype_eat_animal, label="ecarttype_eat_animal")
    plt.hist(animals[:, EAT_ANIMAL], label="eat_animal", bins=20)
    plt.legend()
    plt.figure(8)
    plt.scatter(animals[0:100, HIDE_INDICE], animals[0:100, HEIGHT_INDICE], label="abscisses = hide  ; ordonnées = height")
    plt.legend()
    plt.show()

