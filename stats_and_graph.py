from conf import HEIGHT_INDICE, HIDE_INDICE, THRESHOLD_HUNGRY_TO_HUNT, VALUE_MAX_HIDE, MAX_FOOD, EAT_ANIMAL
import numpy as np

def compute_stats(threshold_to_hunt, animals, height_mean_preys, height_mean_predators, hide_mean,
                  predators_pop, preys_pop, ecarttype_eat_animal, omnis_pop, height_mean_pop):
    total_height_preys = 0
    total_height_pred = 0
    total_height_omni = 0
    total_hide = 0
    total_threshold = 0
    nb_prey = 0
    nb_pred = 0
    nb_omni = 0
    for animal in animals:
        if animal[EAT_ANIMAL] < 0.33:
            total_height_preys += animal[HEIGHT_INDICE]
            total_hide += animal[HIDE_INDICE]
            total_threshold += animal[THRESHOLD_HUNGRY_TO_HUNT]
            nb_prey+=1
        elif animal[EAT_ANIMAL] > 0.66:
            total_height_pred += animal[HEIGHT_INDICE]
            total_threshold += animal[THRESHOLD_HUNGRY_TO_HUNT]
            nb_pred+=1
        else:
            total_height_omni += animal[HEIGHT_INDICE]
            total_threshold += animal[THRESHOLD_HUNGRY_TO_HUNT]
            nb_omni+=1
    ecarttype_eat_animal.append(np.var(animals[:,EAT_ANIMAL])/animals.shape[0])
    predators_pop.append(nb_pred)
    preys_pop.append(nb_prey)
    omnis_pop.append(nb_omni)
    height_mean_preys.append(total_height_preys / (max(1, preys_pop[-1])))
    height_mean_pop.append(total_height_omni / (max(1, omnis_pop[-1])))
    height_mean_predators.append(total_height_pred / (max(1, predators_pop[-1])))
    hide_mean.append(total_hide / max(1, preys_pop[-1]))
    threshold_to_hunt.append(total_threshold / max(1, predators_pop[-1]))

    return threshold_to_hunt, height_mean_preys, height_mean_predators, hide_mean, predators_pop, preys_pop, ecarttype_eat_animal, omnis_pop, height_mean_pop


def color_animal(animals):
    colors = []
    sizes = []
    for animal in animals:
        if animal[EAT_ANIMAL] < 0.33:
            colors.append([0, max(0.2, animal[HIDE_INDICE] / VALUE_MAX_HIDE), 0])
        elif animal[EAT_ANIMAL] > 0.66:
            colors.append(
                [animal[EAT_ANIMAL], 0, 0])  # the more hunter you are, the more you're red
        else:
            colors.append([0, 0, animal[EAT_ANIMAL]])
        sizes.append(animal[HEIGHT_INDICE] ** 2 / 5)
    return (colors, sizes)
