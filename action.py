from random import randint, uniform

import numpy as np

from conf import MOVE_VALUE, X_INDICE, COORD_MAX_X, Y_INDICE, COORD_MAX_Y, FOOD_INDICE, EAT_VALUE, MAX_FOOD, \
    THRESHOLD_HUNGRY_TO_HUNT, VALUE_MAX_HIDE, HIDE_INDICE, CAN_HIDE, HEIGHT_INDICE, \
    HEIGHT_MAX_VALUE, MAX_ANIMALS, NB_CHILD_ANIMALS, COLD_EFFECT_MAX_LEFT, MIN_COLD_EFFECT, \
    COLD_EFFECT_MAX_RIGHT, EAT_ANIMAL, PROBA_TO_NOT_MAKE_CHILD, BONUS_TO_BE_CONSIDERED_AS_PREY, OPTI_ANIMALS, \
    USE_OPTI_ANIMALS, MIN_FOOD_LOSS


def move_and_check(coord, max):
    coord = coord + randint(-MOVE_VALUE, MOVE_VALUE)
    if coord < 0:
        coord = 0
    elif coord > max:
        coord = max
    return coord


def aleatory_move(animals):
    for i in range(len(animals)):
        animals[i][X_INDICE] = move_and_check(animals[i][X_INDICE], COORD_MAX_X)
        animals[i][Y_INDICE] = move_and_check(animals[i][Y_INDICE], COORD_MAX_Y)
    return animals


def check_equal_list(l1, l2):
    for i in range(len(l1)):
        if l1[i] != l2[i]:
            return False
    return True


def create_new_list_animals(animals, animals_to_delete):
    new_animals = []
    for animal in animals:
        to_delete = False
        for animal_to_delete in animals_to_delete:
            if check_equal_list(animal, animal_to_delete):
                to_delete = True
                break
        if not to_delete:
            new_animals.append(animal)
    return new_animals


def predator_eat(predator):
    predator[FOOD_INDICE] += EAT_VALUE
    if predator[FOOD_INDICE] > MAX_FOOD:
        predator[FOOD_INDICE] = MAX_FOOD
    return predator


def predator_decide_to_attack(predator, prey):
    return predator[FOOD_INDICE] < predator[THRESHOLD_HUNGRY_TO_HUNT] and uniform(0, predator[EAT_ANIMAL]) > prey[EAT_ANIMAL]+BONUS_TO_BE_CONSIDERED_AS_PREY
    #return predator[FOOD_INDICE] < predator[THRESHOLD_HUNGRY_TO_HUNT] and\
    #       uniform(0, predator[EAT_ANIMAL])**2 > uniform(0, prey[EAT_ANIMAL]+BONUS_TO_BE_CONSIDERED_AS_PREY)**2
def predators_find_preys(animals):
    preys_to_delete = []
    predators_to_delete = []
    for indice_predator, predator in enumerate(animals):
        for indice_prey, prey in enumerate(animals):
            if predator[X_INDICE] == prey[X_INDICE] and predator[Y_INDICE] == prey[
                Y_INDICE] and predator_decide_to_attack(predator, prey):
                # the predator found a prey
                if uniform(0, VALUE_MAX_HIDE) < prey[HIDE_INDICE] and CAN_HIDE:
                    # the prey decide to try to hide himself
                    if uniform(0, predator[HEIGHT_INDICE]) < uniform(0, prey[HEIGHT_INDICE]):
                        # you're too tall to hide yourself => you die ; a big predator has a bad view
                        preys_to_delete.append(prey)
                        animals[indice_predator] = predator_eat(animals[indice_predator])
                elif uniform(0, predator[HEIGHT_INDICE] ** 2) > uniform(0, prey[HEIGHT_INDICE] ** 2):
                    preys_to_delete.append(prey)
                    animals[indice_predator] = predator_eat(animals[indice_predator])
                else:
                    predators_to_delete.append(predator)

    return np.array(create_new_list_animals(animals, predators_to_delete))


def respect_min_max_attribute(value, min, max):
    if value > max:
        return max
    elif value < min:
        return min
    return value

def are_similar(animal1, animal2):
    coeff = (abs((animal1[HEIGHT_INDICE] - animal2[HEIGHT_INDICE])/HEIGHT_MAX_VALUE) + abs(animal1[EAT_ANIMAL] - animal2[EAT_ANIMAL]))/4
    #coeff = abs(animal1[EAT_ANIMAL] - animal2[EAT_ANIMAL])
    return coeff < uniform(0,0.5)

def make_child(animals):
    new_animals = []
    for animal in animals:
        for animal2 in animals:
            if randint(0, PROBA_TO_NOT_MAKE_CHILD) == 1 :
                break
            if not check_equal_list(animal, animal2) and animal[X_INDICE] == animal2[X_INDICE] and animal[Y_INDICE] == \
                    animal2[Y_INDICE] and are_similar(animal, animal2):
                x = animal[X_INDICE] + randint(-2, 2)
                y = animal[Y_INDICE] + randint(-2, 2)
                height = (animal[HEIGHT_INDICE] + animal2[HEIGHT_INDICE]) / 2 + uniform(-2, 2)
                if (height < 0):
                    height = 0
                elif (height > HEIGHT_MAX_VALUE):
                    height = HEIGHT_MAX_VALUE
                # NOTE : WE USE HIDE_INDICE, BUT FOR PREDATOR THIS INDICE CORRESPOND TO HUNGRY
                hide = (animal[HIDE_INDICE] + animal2[HIDE_INDICE]) / 2 + uniform(-2, 2)
                food_indice = (animal[FOOD_INDICE] + animal2[FOOD_INDICE]) / 4
                hungry_hunt = (animal[THRESHOLD_HUNGRY_TO_HUNT] + animal2[THRESHOLD_HUNGRY_TO_HUNT]) / 2 + uniform(
                        -2, 2)
                hungry_hunt = respect_min_max_attribute(hungry_hunt, 0, MAX_FOOD)
                if (hide < 0):
                    hide = 0
                elif (hide > VALUE_MAX_HIDE):
                    hide = VALUE_MAX_HIDE
                if (int(height) > HEIGHT_MAX_VALUE):
                    print("ALERT :", height)
                eat_animal = (animal[EAT_ANIMAL] + animal2[EAT_ANIMAL]) / 2 + uniform(-0.05, 0.05)
                eat_animal = respect_min_max_attribute(eat_animal, 0, 1)
                if animals.shape[0] < MAX_ANIMALS:
                    new_animals.append([x, y, height, food_indice, hungry_hunt, hide, eat_animal])
                    if NB_CHILD_ANIMALS == 2:
                        new_animals.append([x + 2, y - 2, height, food_indice, hungry_hunt, hide, eat_animal])
                    if NB_CHILD_ANIMALS == 3:
                        new_animals.append([x + 2, y + 2, height, food_indice, hungry_hunt, hide, eat_animal])
    if len(new_animals) == 0:
        return animals
    return np.append(animals, new_animals, axis=0)


def make_predator_hungry(predators, pop_animals, aleatory_factor):
    predators_to_delete = []
    bonus_malus_prey = 1
    bonus_malus_prey = bonus_malus_prey / OPTI_ANIMALS * max(100, pop_animals)
    for predator_key, predator in enumerate(predators):
        #if predators[predator_key, EAT_ANIMAL] > 0.66:
        predators[predator_key, FOOD_INDICE] -= (compute_cold_effect(predators[predator_key]) * (predators[predator_key, EAT_ANIMAL]**0.5 + MIN_FOOD_LOSS)) * aleatory_factor * bonus_malus_prey**USE_OPTI_ANIMALS
        if predator[FOOD_INDICE] < 0:
            predators_to_delete.append(predator)
    return np.array(create_new_list_animals(predators, predators_to_delete))


def compute_cold_effect(animal):
    x_coord = animal[X_INDICE]
    if x_coord <= COORD_MAX_X / 2:
        cold_effect = (COLD_EFFECT_MAX_LEFT - MIN_COLD_EFFECT) * x_coord / (COORD_MAX_X / 2) + MIN_COLD_EFFECT
    else:
        cold_effect = (COLD_EFFECT_MAX_RIGHT - COLD_EFFECT_MAX_LEFT) * (
                x_coord / (COORD_MAX_X / 2) - 1) + COLD_EFFECT_MAX_LEFT
    return cold_effect
