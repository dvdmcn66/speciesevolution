# speciesEvolution



## Getting started

Developped with Python 3.8
Run main.py to see the simulator running. You can configure your simulation settings in conf.py.

The concept is to simulate the evolution of species/individuals in an environment.

The following features have been developed:

-> Individuals move randomly

-> Individuals have characteristics: Sizes, willingness to eat (satiety or not), carnivorism/herbivory index, tendency to hide or fight in case of conflict and a more or less large fat reserve.

-> When two individuals meet, if one is much more carnivorous than the other, it may try to eat it. In this case, the other can decide to hide (probability according to his character), but if he fails he dies. The bigger it is the harder it is to hide, the bigger the predator the easier it is to hide.
Otherwise he can decide to fight, the chance of victory depends on the size of the two. The loser dies. If the hunter wins, he accumulates food.
In an encounter, two individuals similar in their carnivorous trait may decide to breed.

They will give birth to one or more individuals. (configurable). These individuals inherit their average traits with some randomness.

-> There is a gradient of cold from west to east (+ cold in the east). The cold causes more caloric consumption.